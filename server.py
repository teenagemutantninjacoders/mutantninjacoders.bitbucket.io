import http.server
import socketserver
import os

PORT = 4000

web_dir = os.path.dirname(__file__)
#os.chdir(web_dir)

Handler = http.server.SimpleHTTPRequestHandler
httpd = socketserver.TCPServer(("",PORT), Handler)
print("Serving directory " + web_dir + " at port " + str(PORT))
httpd.serve_forever()
